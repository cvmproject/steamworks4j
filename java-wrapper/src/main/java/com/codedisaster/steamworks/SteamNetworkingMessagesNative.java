package com.codedisaster.steamworks;

public class SteamNetworkingMessagesNative {
    /*JNI
		#include <steam_api.h>
		#include "SteamNetworkingMessagesCallback.h"
	*/

    static native long createCallback(SteamNetworkingMessagesCallback javaCallback); /*
		return (intp) new SteamNetworkingMessagesCallback(env, javaCallback);
	*/

    static native boolean sendMessageToUserNative(long steamID, byte[] message, int length, int channel); /*
        SteamNetworkingIdentity identity;
        identity.m_eType = k_ESteamNetworkingIdentityType_SteamID;
        identity.SetSteamID64(steamID);

        EResult result = SteamNetworkingMessages()->SendMessageToUser(identity, message, length, k_nSteamNetworkingSend_UnreliableNoNagle, channel);
        return result == k_EResultOK;
    */

    static native int receiveMessages(int maxMessages, SteamNetworkingMessages.IncomingSteamMessage[] messagesOut, int channel); /*
        SteamNetworkingMessage_t** messages = new SteamNetworkingMessage_t* [maxMessages];

        int receivedCount = SteamNetworkingMessages()->ReceiveMessagesOnChannel(channel, messages, maxMessages);

        jclass incomingSteamMessageClass = env->FindClass("com/codedisaster/steamworks/SteamNetworkingMessages$IncomingSteamMessage");
        jfieldID steamIDField = env->GetFieldID(incomingSteamMessageClass , "remoteSteamID", "J");
        jfieldID dataField = env->GetFieldID(incomingSteamMessageClass , "data", "[B");

        for (int i = 0; i < receivedCount; i ++) {
            jobject incomingSteamMessage = env->AllocObject(incomingSteamMessageClass);
            env->SetLongField(incomingSteamMessage, steamIDField, messages[i]->m_identityPeer.GetSteamID64());

            jbyteArray data = env->NewByteArray(messages[i]->m_cbSize);
            env->SetByteArrayRegion(data,0,messages[i]->m_cbSize,(jbyte*)messages[i]->m_pData);
            env->SetObjectField(incomingSteamMessage, dataField, data);

            env->SetObjectArrayElement(messagesOut, i, incomingSteamMessage);
            messages[i]->Release();
        }

        delete[] messages;
        return receivedCount;
    */

    static native boolean acceptSessionWithUserNative(long steamID); /*
        SteamNetworkingIdentity identity;
        identity.m_eType = k_ESteamNetworkingIdentityType_SteamID;
        identity.SetSteamID64(steamID);

        return SteamNetworkingMessages()->AcceptSessionWithUser(identity);
    */
}
