package com.codedisaster.steamworks;

import java.nio.ByteBuffer;

@SuppressWarnings({ "unused", "UnusedReturnValue" })
public class SteamNetworkingMessages extends SteamInterface {
    public SteamNetworkingMessages(SteamNetworkingMessagesCallback callback) {
        super(SteamNetworkingMessagesNative.createCallback(callback));
    }

    public boolean sendMessageToUser(long steamID, byte[] message, int channel) {
        return SteamNetworkingMessagesNative.sendMessageToUserNative(steamID, message, message.length, channel);
    }

    public boolean acceptSessionWithUser(long steamID) {
        return SteamNetworkingMessagesNative.acceptSessionWithUserNative(steamID);
    }

    public IncomingSteamMessage[] receiveMessages(int maxMessages, int channel) {
        IncomingSteamMessage[] messages = new IncomingSteamMessage[maxMessages];
        int actuallyReceivedCount = SteamNetworkingMessagesNative.receiveMessages(maxMessages, messages, channel);
        return messages;
    }

    public static class IncomingSteamMessage {
        long remoteSteamID;
        byte[] data;

        public long getRemoteSteamID() { return remoteSteamID; }

        public byte[] getData() { return data; }
    }
}
