package com.codedisaster.steamworks;

public class SteamNetworkingUtilsCallbackAdapter extends SteamCallbackAdapter<SteamNetworkingUtilsCallback> {
    SteamNetworkingUtilsCallbackAdapter(SteamNetworkingUtilsCallback callback) {
        super(callback);
    }

    void onSteamRelayNetworkStatusChanged() {
        callback.onSteamRelayNetworkStatusChanged();
    }
}
