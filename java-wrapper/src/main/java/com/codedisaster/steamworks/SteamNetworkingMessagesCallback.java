package com.codedisaster.steamworks;

public interface SteamNetworkingMessagesCallback {
    default void onSessionRequest(long steamID) {}
}
