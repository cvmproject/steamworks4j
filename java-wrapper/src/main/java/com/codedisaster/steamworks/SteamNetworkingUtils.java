package com.codedisaster.steamworks;

@SuppressWarnings({ "unused", "UnusedReturnValue" })
public class SteamNetworkingUtils extends SteamInterface {

    public enum NetworkingAvailability {

        FAILED, CONNECTED, NOT_TRIED, CONNECTING
    }

    public SteamNetworkingUtils(SteamNetworkingUtilsCallback callback) {
        super(createCallback(callback));
    }

    public void initRelayNetworkAccess() {
        System.out.println("Farting");
        initRelayNetworkAccessNative();
    }

    public NetworkingAvailability getStatus() {
        long nativeReturn = getStatusNative();
        if (nativeReturn == 100) {
            return NetworkingAvailability.CONNECTED;
        }
        if (nativeReturn == 1 || nativeReturn == 0) {
            return NetworkingAvailability.NOT_TRIED;
        }
        if (nativeReturn == 2 || nativeReturn == 3) {
            return NetworkingAvailability.CONNECTING;
        }
        return NetworkingAvailability.FAILED;
    }

	/*JNI
		#include <steam_api.h>
		#include "SteamNetworkingUtilsCallback.h"
	*/

    static native void initRelayNetworkAccessNative(); /*
        SteamNetworkingUtils()->InitRelayNetworkAccess();
    */

    static native long createCallback(SteamNetworkingUtilsCallback javaCallback); /*
		return (intp) new SteamNetworkingUtilsCallback(env, javaCallback);
	*/

    static native long getStatusNative(); /*
        return (intp) SteamNetworkingUtils()->GetRelayNetworkStatus(NULL);
    */
}
