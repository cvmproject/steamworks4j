#pragma once

#include "SteamCallbackAdapter.h"
#include <steam_api.h>

class SteamNetworkingUtilsCallback : public SteamCallbackAdapter {

public:
	SteamNetworkingUtilsCallback(JNIEnv* env, jobject callback);
	~SteamNetworkingUtilsCallback();

//    STEAM_CALLBACK(SteamNetworkingUtilsCallback, onSteamRelayNetworkStatusChanged, GameOverlayActivated_t, m_SteamRelayNetworkStatusChanged);
};
