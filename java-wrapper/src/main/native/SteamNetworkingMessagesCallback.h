#pragma once

#include "SteamCallbackAdapter.h"
#include <steam_api.h>

class SteamNetworkingMessagesCallback : public SteamCallbackAdapter {

public:
	SteamNetworkingMessagesCallback(JNIEnv* env, jobject callback);
	~SteamNetworkingMessagesCallback();

    STEAM_CALLBACK(SteamNetworkingMessagesCallback, onSessionRequest, SteamNetworkingMessagesSessionRequest_t);
};
