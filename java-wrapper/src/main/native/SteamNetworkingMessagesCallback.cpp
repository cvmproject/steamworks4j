#include "SteamNetworkingMessagesCallback.h"

SteamNetworkingMessagesCallback::SteamNetworkingMessagesCallback(JNIEnv* env, jobject callback)
	: SteamCallbackAdapter(env, callback) {

}

SteamNetworkingMessagesCallback::~SteamNetworkingMessagesCallback() {

}

void SteamNetworkingMessagesCallback::onSessionRequest(SteamNetworkingMessagesSessionRequest_t* callback) {
    invokeCallback({
        if (callback->m_identityRemote.m_eType == k_ESteamNetworkingIdentityType_SteamID) {
            callVoidMethod(env, "onSessionRequest", "(J)V", (jlong) callback->m_identityRemote.GetSteamID64());
        }
    });
}


